# pr SAL transformations - onetime - extra timed runs

## Baseline hardware in late 2021 and 2022 - labelled m1

In the main sal1520 code directories there is a folder timingconverter which has sample times, conversion factors,  
and defines m1

## Harness scripts - edits to create a harness capable of reproducing the results here

Want ver_minor=599 ?

Make a harnessV53termites41for999triple_trebled599.py by copying then editing harnessV53termites41for999triple_trebled.py

The edit you will need to make above will result in two lines like so:

```
	vermin=599			# ver_minor=599 [ triple trebled ]
	ver=vermaj+0.599
```

Similarly to make a 298 script into an 898 you would aim for those edits but with 898 in place of 599

## Confirming the output timings from the raw output

Lines that start ...
```
## Repeater ends (idx=999) at
```

...contain all the information to calculate the duration [of runtime] in seconds


